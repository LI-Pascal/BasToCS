﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasToCS
{
    class Program
    {
        public enum ReturnValue
        {
            @void,
            @long,
            @int,
            @double,
            @string,
            @bool,
            UNKNOW
        }
        public enum ReturnType
        {
            ByVal,
            ByRef
        }
        public class RetType
        {
            public RetType(string line)
            {
                line = line.Trim();
                var splitLine = line.Split(' ');
                string retour;
                if (splitLine.Count() == 2)
                    retour = line.Split(' ')[1].TrimEnd().TrimStart().ToLower();
                else
                    retour = line.Trim().ToLower();
                if (retour.IndexOf("byv") != -1)
                    Value = ReturnType.ByVal;
                if (retour.IndexOf("byr") != -1)
                    Value = ReturnType.ByRef;
            }
            public ReturnType Value { get; }
        }
        public class RetValue
        {
            public RetValue(string line)
            {
                line = line.Trim();
                if (string.IsNullOrEmpty(line))
                    Value = ReturnValue.@void;
                else
                {
                    string retour = line.Trim().ToLower();
                    if (retour.IndexOf("int") != -1)
                        Value = ReturnValue.@int;
                    if (retour.IndexOf("doub") != -1)
                        Value = ReturnValue.@double;
                    if (retour.IndexOf("str") != -1)
                        Value = ReturnValue.@string;
                    if (retour.IndexOf("int") != -1)
                        Value = ReturnValue.@int;
                    if (retour.IndexOf("bool") != -1)
                        Value = ReturnValue.@bool;
                    if (retour.IndexOf("lon") != -1)
                        Value = ReturnValue.@long;
                }
            }
            public ReturnValue Value { get; }
        }
        public class Var
        {
            public ReturnType returnType_;
            public string name_;
            public ReturnValue type_;
            public Var(string line)
            {
                var lin = line.Split(' ');
                if (lin.Count() == 4)
                {
                    returnType_ = new RetType(lin[0]).Value;
                    name_ = lin[1];
                    type_ = new RetValue(lin[3]).Value;
                }
                else if (lin.Count() == 3)
                {
                    type_ = new RetValue(lin[2]).Value;
                    name_ = lin[0];
                    returnType_ = ReturnType.ByVal;
                }
                else
                {
                    type_ = ReturnValue.UNKNOW;
                    name_ = lin[0];
                    returnType_ = ReturnType.ByVal;
                }
            }
        }
        public class SubKey
        {
            public string functionName { get; }
            public List<Var> varsIn { get; }
            public List<string> resultLines { get; }
            public ReturnValue returnValueType { get; }

            public SubKey(string line, int index, bool Public)
            {
                varsIn = new List<Var>();
                returnValueType = ReturnValue.@void;
                int open = line.IndexOf('(');
                int close = line.IndexOf(')', open);
                if (close != open + 1)
                {
                    var variables = line.Substring(open + 1, close - open - 1).Split(',').ToList();
                    variables = variables.Select(p => p.Trim()).ToList();
                    foreach (string variable in variables)
                    {
                        varsIn.Add(new Var(variable));
                    }
                }
                if (line.Length > close)
                    returnValueType = new RetValue(line.Substring(close + 1)).Value;

                var split_ = line.Substring(0, open).Split(' ').ToList();
                functionName = split_[split_.FindIndex(p => p.StartsWith("Sub")) + 1];

                resultLines = new List<string>();
                int idx = realBasicLines.FindIndex(index, p => p == "End Sub");
                string callingLine = "\t\tPublic " + returnValueType.ToString() + " " + functionName + " (";
                var sepa = "";
                foreach (var var_ in varsIn)
                {
                    callingLine += sepa + var_.returnType_.ToString() + " " + var_.type_.ToString() + " " + var_.name_;
                    sepa = ", ";
                }
                callingLine += ")";
                resultLines.Add(callingLine);
                resultLines.Add("\t\t{");
                for (int i = index + 1; i < idx; i++)
                    resultLines.Add("\t\t\t// " + realBasicLines[i].Trim() + ";");
                resultLines.Add("\t\t}");
            }
        }
        public class DeclareFunctionKey
        {
            public string functionName { get; }
            public List<Var> varsIn { get; }
            public string dllName { get; }
            public ReturnValue returnValueType { get; }
            public DeclareFunctionKey(string line, bool Public, string key = "Function")
            {
                varsIn = new List<Var>();
                functionName = splt[splt.FindIndex(p => p == key) + 1];
                var idx = splt.FindIndex(p => p.StartsWith("Lib")) + 1;
                dllName = splt[idx];
                var open = line.IndexOf('(', idx);
                var close = line.IndexOf(')', open);
                if (close != open + 1)
                {
                    var variables = line.Substring(open + 1, close - open - 1).Split(',').ToList();
                    variables = variables.Select(p => p.Trim()).ToList();
                    foreach (string variable in variables)
                    {
                        varsIn.Add(new Var(variable));
                    }
                }
                if (line.Length > close)
                    returnValueType = new RetValue(line.Substring(close + 1)).Value;
            }
        }
        public class FunctionKey
        {
            public string functionName { get; }
            public List<Var> varsIn { get; }
            public List<string> resultLines { get; }

            public ReturnValue returnValueType { get; }
            public FunctionKey(string line, int index, bool Public)
            {
                varsIn = new List<Var>();
                returnValueType = ReturnValue.@void;
                int open = line.IndexOf('(');
                int close = line.IndexOf(')', open);
                if (close != open + 1)
                {
                    var variables = line.Substring(open + 1, close - open - 1).Split(',').ToList();
                    variables = variables.Select(p => p.Trim()).ToList();
                    foreach (string variable in variables)
                    {
                        varsIn.Add(new Var(variable));
                    }
                }
                if (line.Length > close)
                    returnValueType = new RetValue(line.Substring(close + 1)).Value;

                var split_ = line.Substring(0, open).Split(' ').ToList();
                functionName = split_[split_.FindIndex(p => p.StartsWith("Function")) + 1];

                resultLines = new List<string>();
                int idx = realBasicLines.FindIndex(index, p => p == "End Function");
                string callingLine = "\t\tPublic " + returnValueType.ToString() + " " + functionName + " (";
                var sepa = "";
                foreach (var var_ in varsIn)
                {
                    callingLine += sepa + var_.returnType_.ToString() + " " + var_.type_.ToString() + " " + var_.name_;
                    sepa = ", ";
                }
                callingLine += ")";
                resultLines.Add(callingLine);
                resultLines.Add("\t\t{");
                for (int i = index + 1; i < idx; i++)
                    resultLines.Add("\t\t\t// " + realBasicLines[i].Trim() + ";");
                resultLines.Add("\t\t}");
            }
        }
        public class TypeKey
        {
            public string varName { get; }
            public string varValue { get; }
            public string resultLine { get; }
            public TypeKey(string line, bool Public)
            {
                string variable = line.Split(new string[] { "Type " }, StringSplitOptions.None)[1];
                varName = variable.Split('=')[0].Trim().Trim('\t');
                varValue = variable.Split('=')[1].Trim().Trim('\t');
                resultLine = "\t\t";
                resultLine += Public == true ? "public " : "private ";
                resultLine += "Struct " + varName + " " + varValue + ";";
            }
        }
        public class EnumKey
        {
            public string varName { get; }
            public List<string> resultLines { get; }
            public EnumKey(string line, int index)
            {
                varName = line.Split(new string[] { "Enum " }, StringSplitOptions.None)[1].Trim();
                resultLines = new List<string>();
                int idx = realBasicLines.FindIndex(index, p => p == "End Enum");
                resultLines.Add("\t\tPublic Struct " + varName);
                resultLines.Add("\t\t{");
                for (int i = index + 1; i < idx; i++)
                    resultLines.Add("\t\t\tPublic int " + realBasicLines[i].Trim() + ";");
                resultLines.Add("\t\t}");
            }
        }
        public class ConstKey
        {
            public string varName { get; }
            public string varValue { get; }
            public string resultLine { get; }
            public ConstKey(string line, bool Public)
            {
                string variable = line.Split(new string[] { "Const " }, StringSplitOptions.None)[1];
                varName = variable.Split('=')[0].Trim().Trim('\t');
                varValue = variable.Split('=')[1].Trim().Trim('\t');
                resultLine = "\t\t";
                resultLine += Public == true ? "public " : "private ";
                resultLine += "Const " + varName + " " + varValue + ";";
            }
        }

        static List<string> splt = new List<string>();
        static List<string> realBasicLines = new List<string>();

        static void Main(string[] args)
        {
            if (args.Count() != 1)
            {
                Error("usage basToCS.exe basicFilePath");
                return;
            }
            string basicFile = args[0];
            if (Path.GetExtension(basicFile).ToLower() != ".bas")
            {
                Error("Only .bas file can be processed");
                return;
            }
            string csFile = "";
            if (File.Exists(basicFile))
            {
                csFile = basicFile.Replace(".bas", ".cs");
                List<string> basicLines = File.ReadAllLines(basicFile).ToList();
                string lineTmp = "";
                // Recompose les lignes "multilignes"
                foreach (string line in basicLines)
                {
                    lineTmp += line;
                    lineTmp = lineTmp.Replace("\t", "");
                    if (line.LastIndexOf('_') == -1 || line.LastIndexOf('_') != line.Length - 1)
                    {
                        realBasicLines.Add(lineTmp);
                        lineTmp = "";
                    }
                    else
                        lineTmp = lineTmp.Trim('_').Trim(' ');
                }

                int idx = -1;

                string className = string.Empty;
                var vbName = realBasicLines.Where(p => p.IndexOf("Attribute VB_Name") > -1);
                if (vbName.Count() > 0)
                {
                    className = vbName.First();
                    className = className.Split('\"')[1];
                }
                else
                    className = Path.GetFileNameWithoutExtension(basicFile);

                List<string> exportCs = new List<string>();
                exportCs.Add("using System.Runtime.InteropServices;");
                exportCs.Add("using System;");
                exportCs.Add("");
                exportCs.Add("namespace Actcut");
                exportCs.Add("{");
                exportCs.Add("\tpublic class " + className);
                exportCs.Add("\t{");

                foreach (string line in realBasicLines)
                {
                    idx++;
                    if (line.Trim().StartsWith("End Enum") | line.Trim().StartsWith("End Function") | line.Trim().StartsWith("End Sub"))
                        continue;
                    string line_ = line.Trim();
                    if (string.IsNullOrEmpty(line_))
                    {
                        exportCs.Add("");
                        continue;
                    }

                    bool public_ = false;
                    TypeKey typeKey = null;
                    ConstKey constKey = null;
                    EnumKey enumKey = null;
                    DeclareFunctionKey declareFunctionKey = null;
                    FunctionKey functionKey = null;
                    SubKey subKey = null;

                    splt = line.Split(' ').ToList();
                    splt.RemoveAll(p => string.IsNullOrEmpty(p));
                    if (line.IndexOf("'") == 0 | line.ToLower().IndexOf("rem") == 0)
                    {
                        exportCs.Add("\t\t" + line.Replace("\'", "//"));
                        continue;
                    }

                    if (SearchKey(line, "Public"))
                    {
                        public_ = true;
                    }

                    if (SearchKey(line, "Const"))
                    {
                        constKey = new ConstKey(line_, public_);
                        exportCs.Add(constKey.resultLine);
                        continue;
                    }

                    if (SearchKey(line, "Enum"))
                    {
                        enumKey = new EnumKey(line_, idx);
                        exportCs.AddRange(enumKey.resultLines);
                        continue;
                    }

                    if (SearchKey(line, "Declare Function"))
                    {
                        declareFunctionKey = new DeclareFunctionKey(line_, public_);
                        string dllimport = "\t\t[DllImport(" + declareFunctionKey.dllName + ", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = \"" + declareFunctionKey.functionName + "\")]";
                        string callingLine = "\t\tpublic static extern " + declareFunctionKey.returnValueType.ToString() + " " + declareFunctionKey.functionName + "(";
                        string sepa = "";
                        foreach (Var var_ in declareFunctionKey.varsIn)
                        {
                            callingLine += sepa + var_.returnType_.ToString() + " " + var_.type_.ToString() + " " + var_.name_;
                            sepa = ", ";
                        }
                        callingLine += ");";
                        exportCs.Add(dllimport);
                        exportCs.Add(callingLine);
                        continue;
                    }

                    if (SearchKey(line, "Declare Sub"))
                    {
                        declareFunctionKey = new DeclareFunctionKey(line_, public_, "Sub");
                        string dllimport = "\t\t[DllImport(" + declareFunctionKey.dllName + ", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = \"" + declareFunctionKey.functionName + "\")]";
                        string callingLine = "\t\tpublic static extern " + declareFunctionKey.returnValueType.ToString() + " " + declareFunctionKey.functionName + "(";
                        string sepa = "";
                        foreach (Var var_ in declareFunctionKey.varsIn)
                        {
                            callingLine += sepa + var_.returnType_.ToString() + " " + var_.type_.ToString() + " " + var_.name_;
                            sepa = ", ";
                        }
                        callingLine += ");";
                        exportCs.Add(dllimport);
                        exportCs.Add(callingLine);
                        continue;
                    }

                    if (SearchKey(line, "Sub"))
                    {
                        subKey = new SubKey(line_, idx, public_);
                        exportCs.AddRange(subKey.resultLines);
                        continue;
                    }

                    if (SearchKey(line, "Function"))
                    {
                        functionKey = new FunctionKey(line_, idx, public_);
                        exportCs.AddRange(functionKey.resultLines);
                        continue;
                    }

                    if (SearchKey(line, "Type"))
                    {
                        typeKey = new TypeKey(line_, public_);
                        exportCs.Add(constKey.resultLine);
                        continue;
                    }
                }

                exportCs.Add("\t}");
                exportCs.Add("}");
                TextWriter tw = new StreamWriter(csFile);
                foreach (String csLine in exportCs)
                    tw.WriteLine(csLine);
                tw.Close();
                Process p1 = Process.Start(csFile);
                p1.Close();
            }
            else
            {
                Error("File not found");
            }
        }

        private static bool SearchKey(string line, string key)
        {
            return line.IndexOf(key) > -1 && splt.FindIndex(p => p.StartsWith(key.Split(' ')[0])) != -1;
        }

        private static void Error(string error)
        {
            Console.WriteLine(error);
            Console.WriteLine("Press any key !");
            Console.ReadKey(true);
        }
    }
}
